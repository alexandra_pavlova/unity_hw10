using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoundAccelerator : MonoBehaviour
{
    public float power;
    void Start()
    {
        
    }

    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision other)
    {
        Vector3 punchVector = other.transform.position - transform.position;
        other.rigidbody.AddForce(punchVector.normalized * power, ForceMode.Impulse);
    }
}
