using System.Collections;
using System.Collections.Generic;
using Unity.Collections.LowLevel.Unsafe;
using UnityEngine;

public class MovingPanel : MonoBehaviour
{
    public float speed;
    private Vector3 target;
    public GameObject targetPoint1;
    public GameObject targetPoint2;
    void Start()
    {
        target = targetPoint1.transform.position;
    }

    void Update()
    {
        if (transform.position != target)
        {
            transform.position = Vector3.MoveTowards(transform.position, target, Time.deltaTime * speed);
        }
        else
        {
            ChangeTarget();
        }
    }

    void ChangeTarget()
    {
        target = target == targetPoint1.transform.position ? targetPoint2.transform.position : targetPoint1.transform.position;
    }
}
