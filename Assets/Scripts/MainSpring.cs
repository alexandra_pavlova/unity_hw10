using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainSpring : MonoBehaviour
{
    public GameObject target;
    public GameObject startingPoint;
    public float Speed;
    public float returnSpeed;
    public int interval;
    private float currentTime;
    private bool forward = true;
    
    void Start()
    {
        currentTime = 0;
    }

    void Update()
    {
        currentTime += Time.deltaTime;
        if (forward)
        {
            transform.position = Vector3.MoveTowards(transform.position, target.transform.position, Time.deltaTime * Speed);
            if (transform.position == target.transform.position)
            { 
                forward = false;
            }
        }
        else
        {
            transform.position = Vector3.MoveTowards(transform.position, startingPoint.transform.position, Time.deltaTime * returnSpeed);
            if (transform.position == startingPoint.transform.position)
            {
                if (Mathf.RoundToInt(currentTime) % interval == 0)
                {
                    forward = true;
                }
            }
        }

    }
    
}
